// import React, {useEffect, useReducer} from 'react';
import './App.css';
import Navbar from './components/Navbar';
import News from './components/News';
import Login from './components/Login';
import Profile from './components/Profile';
import Home from './components/Home';
import { BrowserRouter as Router, Route, Switch} from 'react-router-dom';

function App() {
  

  return (
    <Router>
      <div className="App">
        <Navbar/>
        <div className="content">
            <Switch>
              <Route exact path='/'> <Home/> </Route>
            </Switch>
            <Switch>
              <Route exact path='/news'> <News/> </Route>
            </Switch>
            <Switch>
              <Route exact path='/profile'> <Profile/> </Route>
            </Switch>
            <Switch>
              <Route exact path='/login'> <Login/> </Route>
            </Switch>
        </div>
      </div>
 </Router>
  );
}

export default App;
