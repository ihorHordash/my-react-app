import React from  'react';
import {useHistory} from 'react-router-dom';
import {useState, useEffect} from 'react';

const Profile = () => {

    const url = 'https://randomuser.me/api/';
    const [error, setError] = useState(null);
    const [isLoading, setIsLoading] = useState(true);
    const [user, setUser] = useState(null);

    const redirect = useHistory();

    useEffect(()=>{
        setTimeout(()=> {
            fetch(url)
        .then( res => res.json())
        .then(
            (result)=> {
                setIsLoading(false);
                setUser(...result['results'])
                // console.log(data)
                },
                (error)=>{
                    setIsLoading(false);
                    setError(error);
                }
            )
        }, 1000)        
    },[url])


    const handleLogout = (e)=>{
        localStorage.setItem('loggedIn','false');
        redirect.push('/');
    }

    return ( 
        <div className="content-wrap profile">

            {localStorage.getItem('loggedIn')==='false'? redirect.push('/login'):''}

            <h2 style={{marginLeft:'10px'}}>Profile information </h2>
            

            {error && <div>{error.message}</div>}
            {isLoading && <div>Loading...</div>}
            {/* {!isLoading && <p>{JSON.stringify(user)}</p>} */}
            {user && <div className='user-info'>
                <h1 style={{textAlign: 'center'}}>{user['name'].first} {user['name'].last}</h1>
                <div className='user-info-head'>
                    <img src={user.picture.medium}alt=""/>
                    <div className='user-info-head-aside'>
                    <p>Gender: {user.gender}</p>
                    <p>Email: {user.email}</p>
                    </div>
                </div>
                <div className='user-info-body'>
                    <h3>Status</h3>
                    <p>" <em>{user.location.city} is the best city in {user.location.country}. Just come and check this out! </em>"</p>
                </div>
                
            </div>}
            {user && <button className='logout-btn' onClick={(event)=>handleLogout(event)}>Logout</button>}
        </div>
     );
}
 
//margin:'10px 3px 0 0'

export default Profile;