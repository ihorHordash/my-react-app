import {Link} from 'react-router-dom';


const Navbar = () => {
    return ( 
            <div>
                <header className="header">
                    <div className="top-menu">
                        
                        <div className="navigation">
                            <Link to="/" className="link">Home</Link>
                            <Link to="/news" className="link">News</Link>
                            <Link to="/profile" className="link">Profile</Link>
                            {localStorage.getItem('loggedIn')==='false'? 
                                                        <Link to="/login" className="link">Login</Link> 
                                                        :<Link to="/login" className="disabledCursour link" onClick={(event) => event.preventDefault()}>Login</Link> }
                        </div>
                    </div>
                </header>
            </div>
     );
}
 
export default Navbar;
