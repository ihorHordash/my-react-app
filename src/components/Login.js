import {useState} from 'react';
import {useHistory} from 'react-router-dom';




const Login = () => {

    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');

    const history = useHistory();

    const handleLoginSubmit = (e) => {
        e.preventDefault();
        username===localStorage.getItem('login')&&password===localStorage.getItem('password')?
                                                        localStorage.setItem('loggedIn',true)
                                                        :alert('Incorrect username or password!');
        if(localStorage.getItem('loggedIn')){
        history.push('/profile');
        }
    }

    


    return ( 
        <div className="login-page">
            
            <form className='login-form'>
                <label>Enter your username
                    <input type="text"
                        required
                        value={username} 
                        onChange={(e) => setUsername(e.target.value)}/>
                </label>
                <label>Enter your password
                    <input type="password" 
                        required
                        value={password}
                        onChange={(e)=>setPassword(e.target.value)}/>
                </label>
                <button type='submit'
                onClick={(e)=> handleLoginSubmit(e)}> Submit </button>
            </form>
        </div>
     );
}
 
export default Login;