import {useState,useEffect} from 'react';

const News = () => {

    const coctailsUrl = 'https://www.thecocktaildb.com/api/json/v1/1/filter.php?c=Cocktail';
    const [isLoading, setIsLoading] = useState(true);
    const [error, setError] = useState(null);
    const [coctailsDB, setCoctailsDB] = useState(null);

    useEffect(()=>{
        fetch(coctailsUrl)
        .then(res => {
            if (!res.ok){
                setError('cannot fetch data from server');
            } else {
            return res.json()}})
        .then(json => {
            setIsLoading(false);
            setCoctailsDB(json['drinks']);
        })
        .catch(error => {
            setIsLoading(false);
            setError(error);
        })
    },[coctailsUrl])

    console.log(coctailsDB)

    const pageInfoRender = () => coctailsDB.map(drink => 
                (<div className='coctail-cards' key={drink['idDrink']}>
                    <h3>{drink['strDrink']}</h3>
                    <img src={drink['strDrinkThumb']} alt='coctail'/>
                    <span>{drink['idDrink']}</span>
                </div>)
            )


    return ( 
        <div className="news-wrap">
            
            {isLoading && <div className='content-wrap'>Loading...</div>}
            {error && <div>{error}</div>}
            
            {coctailsDB && <div className='content-wrap'><h2 id='h2'>Top 100 coctails:</h2>{pageInfoRender(coctailsDB)}</div>}
            
        </div>
    );
}
 
export default News;